from django.db import models
from captcha.fields import CaptchaField

# Create your models here.
class User(models.Model):
	pilih_bank = (
	('BNI','BNI'),
	('BCA','BCA'),
	)


	Username = models.CharField(max_length=128)
	nickname = models.CharField(max_length=128)
	password = models.CharField(max_length=30)
	konfir_password = models.CharField(max_length=30)
	email = models.EmailField(max_length=254,unique=True)
	no_telepon = models.CharField(max_length=20)
	nama_rek_bank = models.CharField(max_length=20)
	nama_bank = models.CharField(max_length=20)
	no_rek_bank = models.CharField(max_length=30)
	referal= models.CharField(max_length=20)
	

	def __str__(self):
		return self.Username
# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-12-19 08:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Username', models.CharField(max_length=128)),
                ('nickname', models.CharField(max_length=128)),
                ('password', models.CharField(max_length=30)),
                ('konfir_password', models.CharField(max_length=30)),
                ('no_telepon', models.CharField(max_length=20)),
                ('nama_rek_bank', models.CharField(max_length=20)),
                ('nama_bank', models.CharField(max_length=20)),
                ('no_rek_bank', models.CharField(max_length=30)),
                ('referal', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=254, unique=True)),
            ],
        ),
    ]

from django import forms
from formulir_bank_1174039.models import User
from captcha.fields import CaptchaField
from django import forms


class NewUserForm(forms.ModelForm):		
	captcha = CaptchaField()
	class Meta:
		model = User
		fields = '__all__'
		
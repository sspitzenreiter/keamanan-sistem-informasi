from django.shortcuts import render
#from django.http import HttpResponse
from ujian_aplikasi_1174039.forms import NewUserForm

# Create your views here.
def index(request):
	return render(request,'ujian_aplikasi_1174039/index_1174039.html')

def users_1174039(request):
	form = NewUserForm()


	if request.method == "POST":
		form = NewUserForm(request.POST)

		if form.is_valid():
			form.save(commit=True)
			return index(request)

		else:
			print("EROR FROM INVALID")
	return render(request,'ujian_aplikasi_1174039/users_1174039.html', {'form':form})
from django import forms
from ujian_aplikasi_1174039.models import User
from captcha.fields import CaptchaField


class NewUserForm(forms.ModelForm):
	captcha = CaptchaField()
	class Meta:
		model = User
		fields = '__all__'
		
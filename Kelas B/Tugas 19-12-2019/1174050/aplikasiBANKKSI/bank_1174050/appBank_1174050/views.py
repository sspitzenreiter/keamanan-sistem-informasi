from django.shortcuts import render
#from django.http import HttpResponse
from appBank_1174050.forms import NewUserForm
from django.views.generic import CreateView
# Create your views here.
def index(request):
    return render(request, 'appBank_1174050/index_1174050.html')

def users_1174050(request):
    form = NewUserForm()
    

    #jika user menekan tombol submit maka
    if request.method == "POST":
    	form = NewUserForm(request.POST)

    	if form.is_valid():
    		form.save(commit=True)
    		return index(request)
    	else:
    		print("ERROR FORM INVALID")
    return render(request,'appBank_1174050/users_1174050.html',{'form':form})

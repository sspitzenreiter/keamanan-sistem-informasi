from django.shortcuts import render
#from django.http import HttpResponse
from bank_app_1174040.forms import NewUserForm

# Create your views here.
def index(request):
	return render(request,'index.html')

def users(request):
	form = NewUserForm()
	

	if request.method == "POST":
		form = NewUserForm(request.POST)

		if form.is_valid():
			form.save(commit=True)
			return index(request)

		else:
			print("EROR FROM INVALID")
	return render(request,'daftar.html', {'form':form})
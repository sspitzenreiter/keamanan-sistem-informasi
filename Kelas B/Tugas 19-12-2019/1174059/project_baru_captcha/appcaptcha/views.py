from django.shortcuts import render
from . import forms

# Create your views here.


def CaptchaField(request):
    response = {}
    data = request.POST
    captcha_rs = data.get('g-recaptcha-response')
    params = {
        'secret': settings.RECAPTCHA_SECRET_KEY,
        'response': captcha_rs,
        'remoteip': get_client_ip(request)
    }
    verify_rs = requests.get(url, params=params, verify=True)
    verify_rs = verify_rs.json()
    response["status"] = verify_rs.get("success", False)
    response['message'] = verify_rs.get(
        'error-codes', None) or "Unspecified error."
    return response


def index(request):
    return render(request, 'appcaptcha/index_1174059.html')


def form_name_view(request):
    form = forms.Formname()
    return render(request, 'appcaptcha/form_page.html', {'form': form})

from django import forms
from appcaptcha.models import User
from django.core import validators


class FormName(forms.Form):
    username = forms.CharField(required=True)
    nickname = forms.CharField(required=True)
    password = forms.CharField(required=True)
    konfpass = forms.CharField(required=True)
    email = forms.EmailField()
    notlp = forms.CharField()
    nama_rek_bank = forms.CharField(required=True)
    nama_bank = forms.CharField(required=True)
    no_rek_bang = forms.CharField(required=True)
    referal = forms.CharField(required=True)
    captcha = CaptchaField(required=True)

    class Meta:
        model = User
        fields = '__all__'

from django.apps import AppConfig


class AppcaptchaConfig(AppConfig):
    name = 'appcaptcha'

from django.shortcuts import render
from FormDaftar_app_1174034.forms import NewUserForm
from django.views.generic import CreateView
# Create your views here.

def index(request):
    return render(request, 'FormDaftar_1174034/index_1174034.html')

def users(request):
    form = NewUserForm()

    if request.method == "POST":
        form = NewUserForm(request.POST)

        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print("ERROR FORM INVALID")
    return render(request, 'FormDaftar_1174034/users_1174034.html',{'form':form})
from django.shortcuts import render
from NajibBankApp_1174042.forms import NewUserForm
from django.views.generic import CreateView
# Create your views here.

def index(request):
    return render(request, 'NajibBank_1174042/index_1174042.html')

def users(request):
    form = NewUserForm()

    if request.method == "POST":
        form = NewUserForm(request.POST)

        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print("ERROR FORM INVALID")
    return render(request, 'NajibBank_1174042/users_1174042.html',{'form':form})
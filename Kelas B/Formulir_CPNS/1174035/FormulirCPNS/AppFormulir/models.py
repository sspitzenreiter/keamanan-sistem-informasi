from django.db import models

# Create your models here.
from django_cryptography.fields import encrypt

class cpns(models.Model):
    instansi_pilih = (
        ('D4 Teknik Informatika','D4 Teknik Informatika'),
        ('D3 Teknik Informatika','D3 Teknik Informatika'),
        ('D4 Logistik Bisnis','D4 Logistik Bisnis'),
        ('D3 Logistik Bisnis','D3 Logistik Bisnis'),
        ('Manajemen Bisnis','Manajemen Bisnis'),
        ('Akuntansi','Akuntansi')
    )
    nik = models.CharField(max_length=40, unique=True)
    nama = models.CharField(max_length=40)
    email = models.CharField(max_length=40)
    tempat_lahir = models.CharField(max_length=40)
    tgl_lahir = models.DateField()
    instansi = models.CharField(max_length=40, choices=instansi_pilih, default="")
    password = encrypt(models.CharField(max_length=40, default=""))
    confirm_password = encrypt(models.CharField(max_length=40, default=""))
    
    def __str__(self):
        return self.nik
    class Meta:
        verbose_name_plural="data_cpns"
from django.shortcuts import render
from AppFormulir.forms import Formulir
# Create your views here.
def index(request):
    form = Formulir()
    if request.method == "POST":
        form = Formulir(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return confirmed(request)
        else:
            print("Form Invalid")
    return render(request, 'form_035.html', {'form':form})

def confirmed(request):
    return render(request, 'confirm.html')
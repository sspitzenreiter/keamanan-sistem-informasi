from django import forms
from AppFormulir.models import cpns
from captcha.fields import CaptchaField
class Formulir(forms.ModelForm):
    captcha = CaptchaField()
    
    def clean(self):
        
        nik = self.cleaned_data.get('nik')
        nik_data = cpns.objects.filter(nik=nik)
        if nik_data.exists():
            raise forms.ValidationError("NIK sudah ada")
        nama = self.cleaned_data.get('nama')
        if nama == "":
            raise forms.ValidationError("Nama tidak boleh kosong")
        tempat_lahir = self.cleaned_data.get('tempat_lahir')
        if tempat_lahir == "":
            raise forms.ValidationError("Tempat lahir tidak boleh kosong")
        tanggal_lahir = self.cleaned_data.get('tanggal_lahir')
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password != confirm_password:
            raise forms.ValidationError("Password Tidak sesuai")

        email = self.cleaned_data.get('email')

    class Meta:
        model=cpns
        fields='__all__'
        widgets={
            'password':forms.PasswordInput(),
            'confirm_password':forms.PasswordInput(),
            'nik': forms.TextInput(attrs={'placeholder': '1174035'}),
			'nama': forms.TextInput(attrs={'placeholder': 'Luthfi Muhammad Nabil'}),
			'email': forms.TextInput(attrs={'placeholder': 'luthfimnabil17@gmail.com'}),
			'tempat_lahir': forms.TextInput(attrs={'placeholder': 'Bandung'}),
			'tanggal_lahir': forms.TextInput(attrs={'placeholder': '02/17/1999'})
        }
    
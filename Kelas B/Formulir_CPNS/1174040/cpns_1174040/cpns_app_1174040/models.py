from django.db import models

# Create your models here.
class Data(models.Model):
    nama_bank = (
    ('D4 Teknik Informatika','D4 Teknik Informatika'),
    ('D3 Teknik Informatika','D3 Teknik Informatika'),
    ('D4 Logistik Bisnis','D4 Logistik Bisnis'),
    ('D3 Logistik Bisnis','D3 Logistik Bisnis'),
    ('Manajemen Bisnis','Manajemen Bisnis'),
    ('Akuntansi','Akuntansi')
    )

    nik = models.CharField(max_length=128, unique=True )
    nama = models.CharField(max_length=128)
    email = models.EmailField(max_length=254,unique=True)
    tempatlahir = models.CharField(max_length=128)
    tanggallahir = models.DateField()
    prodi = models.CharField(max_length=128, choices=nama_bank, default="")
    password = models.CharField(max_length=128)
    confirm_password = models.CharField(max_length=128)

    def __str__(self):
        return self.nik
    
    class Meta:
        verbose_name_plural = "Data"
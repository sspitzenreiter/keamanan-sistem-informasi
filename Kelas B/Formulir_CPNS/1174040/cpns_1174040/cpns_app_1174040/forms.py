from django import forms
from cpns_app_1174040.models import Data
from captcha.fields import CaptchaField


class NewUserForm(forms.ModelForm):
	captcha = CaptchaField()

	def clean(self):
		password = self.cleaned_data.get('password')
		confirm_password = self.cleaned_data.get('confirm_password')
		if password != confirm_password:
			raise forms.ValidationError("Password tidak sesuai")

		email = self.cleaned_data.get('email')
		email_data = Data.objects.filter(email=email)
		if email_data.exists():
			raise forms.ValidationError("Email sudah ada yang memakai")

		nik = self.cleaned_data.get('nik')
		nik_data = Data.objects.filter(nik=nik)
		if nik_data.exists():
			raise forms.ValidationError("NIK Sudah Terdaftar")

		namalengkap = self.cleaned_data.get('namalengkap')
		if namalengkap == "":
			raise forms.ValidationError("Nama tidak boleh kosong")

	class Meta:
		model = Data
		fields = '__all__'
		widgets = {
			'password': forms.PasswordInput(),
			'confirm_password':forms.PasswordInput(),
			'nik': forms.TextInput(attrs={'placeholder': '1174040'}),
			'nama': forms.TextInput(attrs={'placeholder': 'Hagan Rowlenstino Alexander Stevanus'}),
			'email': forms.TextInput(attrs={'placeholder': 'hagansiahaan753@gmail.com'}),
			'tempatlahir': forms.TextInput(attrs={'placeholder': 'Bekasi'}),
			'tanggallahir': forms.TextInput(attrs={'placeholder': '02/15/2000'})
		}
		
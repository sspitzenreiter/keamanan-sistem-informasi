from django.db import models

# Create your models here.
class User(models.Model):
    NPM = models.CharField(max_length=128, default="", unique=True)
    nama_lengkap = models.CharField(max_length=128, unique=True)
    email = models.EmailField(max_length=254, help_text="*Email Harus Aktif", unique=True)
    tempat_lahir = models.CharField(max_length=50)
    tanggal_lahir = models.CharField(max_length=50)
    prodi = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    
   
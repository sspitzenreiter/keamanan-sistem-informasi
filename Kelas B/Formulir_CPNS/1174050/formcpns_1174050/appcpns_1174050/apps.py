from django.apps import AppConfig


class Appcpns1174050Config(AppConfig):
    name = 'appcpns_1174050'

from django import forms
from appcpns_1174050.models import User
from captcha.fields import CaptchaField

Prodi_Choices= [
    ('D4ti', 'D4 Teknik Informatika'),
    ('D4LB', 'D4 Logistik Bisnis'),
    ('D4MB', 'D4 Manajemen Bisnis'),
    ('D4AK', 'D4 Akuntansi'),
    ('D3MI', 'D3 Manajemen Informatika'),
    ('D3ti', 'D3 Teknik Informatika'),
    ('D3LB', 'D3 Logistik Bisnis'),
    ('D3MB', 'D3 Manajemen Bisnis'),
    ('D3AK', 'D3 Akuntansi'),
    ]

class NewUserForm(forms.ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = User
        widgets = {
        'NPM' : forms.TextInput(attrs={'placeholder': 'NPM Anda'}),
        'nama_lengkap' : forms.TextInput(attrs={'placeholder': 'Nama Lengkap Anda'}),
        'email' : forms.TextInput(attrs={'placeholder': 'Email harus Valid dan Benar e.g email_anda@example.com'}),
        'tempat_lahir' : forms.TextInput(attrs={'placeholder': 'Tempat Lahir Anda'}),
        'tanggal_lahir' : forms.TextInput(attrs={'placeholder': 'Tanggal Lahir Anda'}),
        'prodi' : forms.Select(choices=Prodi_Choices),
        'password': forms.PasswordInput(attrs={'placeholder': 'Password Account Anda'}),
        } 
        fields = '__all__'
from django.apps import AppConfig


class FormulirCpns1174039Config(AppConfig):
    name = 'formulir_cpns_1174039'

from formulir_cpns_1174039.models import Users
from captcha.fields import CaptchaField
from django import forms

class NewUserForm(forms.ModelForm):
    kode_security       = CaptchaField()

    def clean(self): 
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if password != confirm_password:
            raise forms.ValidationError("Password tidak sama")
        
        nik = self.cleaned_data.get('nik')
        nik_qs = Users.objects.filter(nik=nik)
        if nik_qs.exists():
            raise forms.ValidationError("NIK Sudah Ada") 
        
        email = self.cleaned_data.get('email')
        email_qs = Users.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError("EMAIL Sudah Digunakan") 

        tempat_lahir = self.cleaned_data.get('tempat_lahir')
        if tempat_lahir == "Bandung":
            raise forms.ValidationError("Daerah Bandung Tidak Boleh Mendaftar") 

        nama_lengkap = self.cleaned_data.get('nama_lengkap')        
        if nama_lengkap[0].lower() != 'a':
            raise forms.ValidationError("Nama Harus lengkap")
        

    
    class Meta:
        model = Users
        fields = '__all__'
        widgets = {
        'nik' : forms.TextInput(attrs={'class':'form-control'}),
        'nama_lengkap' : forms.TextInput(attrs={'class':'form-control'}),
        'email' : forms.TextInput(attrs={'class':'form-control'}),
        'tempat_lahir' : forms.TextInput(attrs={'class':'form-control'}),
        'tanggal_lahir' : forms.DateInput(attrs={'class':'form-control'}),
        'instansi' : forms.Select(attrs={'class':'form-control'}),
        'password' : forms.PasswordInput(attrs={'class':'form-control'}),
        'confirm_password' : forms.PasswordInput(attrs={'class':'form-control'}),
        }
        
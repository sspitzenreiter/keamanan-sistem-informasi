from django.db import models
from captcha.fields import CaptchaField

# Create your models here.
class Users(models.Model):
    pilih_prodi = (
    ('Teknik Informatika','Teknik Informatika'),
    ('MB', 'Manajemen Bisnis'),
    ('AK','Akuntansi'),
    )   
  
    nik = models.CharField(max_length=264)
    nama_lengkap = models.CharField(max_length=254)
    email = models.CharField(max_length=254)
    tempat_lahir = models.CharField(max_length=264)
    tanggal_lahir = models.DateField(help_text="YYYY-MM-DD (1987-12-10)")
    prodi = models.CharField(max_length=20, choices=pilih_prodi, default='')
    password = models.CharField(max_length=30)
    confirm_password = models.CharField(max_length=30)
    
    def __str__(self):
        return self.nik
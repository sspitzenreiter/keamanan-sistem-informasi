from django.shortcuts import render
from django.http import HttpResponse
from appacpns059.forms import UserForm

# Create your views here.


def index(request):
    return render(request, 'index059.html')


def halform(request):
    form = UserForm()
    if request.method == "POST":

        form = UserForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
        else:
            print("ERROR FORM INVALID")
    return render(request, 'form059.html', {'form': form})

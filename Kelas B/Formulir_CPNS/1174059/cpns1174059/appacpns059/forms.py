from django import forms
from appcpns059.models import User059
from captcha.fields import CaptchaField


class UserForm(forms.ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = User059
        fields = '__all__'

        widgets = {
            'password': forms.PasswordInput(),
            'confirm_password' : forms.PasswordInput()
        }

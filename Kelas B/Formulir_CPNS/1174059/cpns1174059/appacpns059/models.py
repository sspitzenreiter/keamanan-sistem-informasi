from django.db import models
from django_countries.fields import CountryField
import pytz

# Create your models here.


class User059(models.Model):
    timezone = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    nik = models.CharField(max_length=16, required=True)
    nama_lengkap = models.CharField(max_length=255, required=True)
    email = models.EmailField(max_length=255, unique=True, required=True)
    password = models.CharField(max_length=255)
    confrim_password = models.CharField(max_length=255, default="")

    def __str__(self):
        return "{}".format(self.name)

from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174039.models import User

# Create your views here.
def index(request):
	return render(request,'ujian_aplikasi_1174039/index_1174039.html')

def users_1174039(request):
	user = User.objects.all()
	userdict = {'users':user}
	return render(request,'ujian_aplikasi_1174039/users_1174039.html',context=userdict)
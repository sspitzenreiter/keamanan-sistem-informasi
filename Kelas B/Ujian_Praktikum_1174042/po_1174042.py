import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','Ujian_Praktikum_1174042.settings')
import django
django.setup()
import random
from Ujian_Aplikasi_1174042.models import User
from faker import Faker

fakegen = Faker()
users = ['Faisal','Najib','Abdullah','Andre','Dika']

def populate(N=30):
    for entry in range(N):
        fake_lastname = fakegen.last_name()
        fake_email = fakegen.email()

        user= User.objects.get_or_create(first_name=random.choice(users),last_name=fake_lastname,email=fake_email)[0]

if __name__=='__main__':
    print("Populating the database.....Please Wait!")
    populate()
    print("Populating Complete!")
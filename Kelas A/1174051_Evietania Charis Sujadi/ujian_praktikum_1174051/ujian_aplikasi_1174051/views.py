from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174051.models import User
# Create your views here.
def index(request):
    return render(request,'ujian_aplikasi_1174051/index_1174051.html')

def user_1174051(request):
    user = User.objects.all()
    userdict = {'users': user}
    return render(request,'ujian_aplikasi_1174051/user_1174051.html',context= userdict)

from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174006.models import User

# Create your views here.
def index(request):
	return render(request,'ujian_aplikasi_1174006/index_1174006.html')

def user_1174006(request):
    user = User.objects.all()
    userdict = {'users': user}
    return render(request,'ujian_aplikasi_1174006/users_1174006.html',context= userdict)
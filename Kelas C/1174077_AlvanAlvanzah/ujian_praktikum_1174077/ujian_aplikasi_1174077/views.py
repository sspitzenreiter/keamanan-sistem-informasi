from django.shortcuts import render
from django.http import HttpResponse
from .models import Users

# Create your views here.

def index(request):
    return render (request,'ujian_aplikasi_1174077/index_1174077.html')

def user_detail(request):
    user = Users.objects.all()
    context = {
        'User':user,
    }
    return render(request, 'ujian_aplikasi_1174077/users_1174077.html', context)
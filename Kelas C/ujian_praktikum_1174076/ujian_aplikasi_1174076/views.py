from django.shortcuts import render
from django.http import HttpResponse
from ujian_aplikasi_1174076.models import User

# Create your views here.

def index(request):
    judul_index = {'judul':'Index Ujian Praktikum Difa_1174076'}
    return render(request, 'ujian_aplikasi_1174076/index_1174076.html',context=judul_index)

def users_1174076(request):
    data = User.objects.all()
    return render(request, 'ujian_aplikasi_1174076/users_1174076.html',{'data':data})


# -*- coding: utf-8 -*-
"""
Created on Sun Nov 10 15:37:44 2019

@author: Aulyardha Anindita
"""

import os
#configure settings for project
# Need to run this before models from application!
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174054.settings')

import django
#import settings
django.setup()

import random
from ujian_aplikasi_1174054.models import User
from faker import Faker

fakegen = Faker()

def po_1174054(N=30):
    for entry in range (N):
        
        #Buat data menggunakan faker
        fake_first_name = fakegen.first_name()
        fake_last_name = fakegen.last_name_male()
        fake_email = fakegen.email()
        
        #Memasukkan data ke user
        pengguna = User.objects.get_or_create(first_name=fake_first_name, last_name=fake_last_name, email=fake_email)[0]
        
if __name__=='__main__':
    print("Populating the database....... Please wait!")
    po_1174054(30)
    print("Populating Complete")